# avaJ

avaJ is a school project written in Java 
<br>
It's a text based flight simulation program where you give it a scene and it will run that scene, moving the aircrafts around and letting you know if any crash (or land safely)


We where given a UML diagram and had to make the program in Java following the Gang of Four design patterns

## UML Diagram
![UML](https://gitlab.com/Heagan/avaJ/raw/master/avaj_uml.jpg "UML DIAGRAM")

# Example 
## You run it with a scene like this below
It will simulate the first 25 ticks

<br>25
<br>Baloon B1 2 3 20
<br>Baloon B2 1 8 66
<br>JetPlane J1 23 44 32
<br>Helicopter H1 654 33 20


## Result with just the Balloon

<br>Balloon#B1 successfully registered!
<br>Balloon#B1(1): Hope the snow cushions our landing!
<br>Balloon#B1(1): Damn you rain! You messed up my balloon.
<br>Balloon#B1(1): Damn you rain! You messed up my balloon.
<br>Balloon#B1(1): Damn you rain! You messed up my balloon.
<br>Balloon#B1(1): Wheres the ground? Too foggy!
<br>Balloon#B1(1): Hope the snow cushions our landing!
<br>Balloon#B1(1): Let's enjoy the good weather and take some pics.
<br>Balloon#B1(1): Wheres the ground? Too foggy!
<br>Balloon#B1(1): Damn you rain! You messed up my balloon.
<br>Balloon#B1(1): Landing.
<br>Balloon#B1(1): unregistered from weather tower.



