package avaj;

public class                        WeatherProvider {
    private static WeatherProvider  weatherProvider;
    private static String           weather;

    public static WeatherProvider getProvider() {
        if (weatherProvider == null)
            weatherProvider = new WeatherProvider();
        return (weatherProvider);
    }

    public String getCurrentWeather(Coordinates coordinates) {
        double s_in;
        double c_os;
        double t_an;
        int    degree;

        s_in = Math.sin(coordinates.getLongitude());
        c_os = Math.cos(coordinates.getLatitude());
        t_an = Math.tan(coordinates.getHeight()) + 1;
        degree = (int)((s_in + c_os + t_an) * 100);
        degree %= 4;
        switch (degree) {
            case (0): {
                weather = "SUN";
            } break ;
            case (1): {
                weather = "RAIN";
            } break ;
            case (2): {
                weather = "FOG";
            } break ;
            case (3): {
                weather = "SNOW";
            } break ;
            default: {
                if (degree > 3)
                    throw new UnknownError("GEORGE YOU FUCKED UP THE MATH! " + degree);
            }
        }
        return (weather);
    }
}
