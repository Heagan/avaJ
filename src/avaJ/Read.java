package avaj;
import java.io.*;

public class Read {
	public static String line = null;
	public static BufferedReader bufferedReader;
    static String fileName;
    static FileReader fileReader;

	public static void     openFile( String file ) {

		File pfile = new File(file);
		if (!pfile.exists())
			throw new FileDoesntExist("File specified dosnt exist!");
		if (pfile.isDirectory())
			throw new FileDoesntExist("File specified is a directory!");
		try {
			fileName = file;
            fileReader = new FileReader(fileName);
            bufferedReader = new BufferedReader(fileReader);
		}
		catch(FileNotFoundException ex) {
			throw new FileOpeningError("Unable to open file '" + fileName + "'");
		}
	}

	public static String readLn() {
		try {
			while ((line = bufferedReader.readLine()) != null) {
				return (line);
			}
		}
		catch (IOException ex) {
			throw new FileOpeningError("Error reading file '" + fileName + "'");
		}
		return (null);
	}

	public static void closeFile() {
        try {
            bufferedReader.close();
        }
        catch (IOException ex) {
            throw new FileOpeningError("Error closing file '" + fileName + "'");
        }
    }
}
