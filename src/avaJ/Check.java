package avaj;

public class Check {
    public static boolean  issDigit(String s) {
        for (char c: s.toCharArray()) {
            if (!Character.isDigit(c))
                return (false);
        }
        return (true);
    }
}
