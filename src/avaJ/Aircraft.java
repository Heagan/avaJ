package avaj;

public class Aircraft {
    protected long id;
    protected String name;
    protected Coordinates coordinates;
    private static long idCounter;

    protected Aircraft(String name, Coordinates coordinates) {
        this.name = name;
        this.coordinates = coordinates;
        this.id = nextId();
    }

    private long nextId() {
        return (++Aircraft.idCounter);
    }
}

class Balloon extends Aircraft implements Flyable {
    private WeatherTower weatherTower;

    Balloon(String name, Coordinates coordinates) {
        super(name, coordinates);
    }

    private boolean heightCheck() {
        if (this.coordinates.getHeight() <= 0) {
            Write.writeLn("Balloon#" + name + "(" + id + "): Landing.");
            weatherTower.unregister(this);
            Write.writeLn("Balloon#" + name + "(" + id + "): unregistered from weather tower.");
            return (true);
        }
        return (false);
    }

    public void updateConditions() {
        Write.write("Balloon#" + name + "(" + id + "): ");
        switch (weatherTower.getWeather(this.coordinates)) {
            case ("SUN"): {
                this.coordinates.setLongitude(this.coordinates.getLongitude() + 2);
                this.coordinates.setHeight(this.coordinates.getHeight() + 4);
                Write.writeLn("Let's enjoy the good weather and take some pics.");
                heightCheck();
            }
            break;
            case ("RAIN"): {
                this.coordinates.setHeight(this.coordinates.getHeight() - 5);
                Write.writeLn("Damn you rain! You messed up my balloon.");
                heightCheck();
            }
            break;
            case ("FOG"): {
                this.coordinates.setHeight(this.coordinates.getHeight() - 3);
                Write.writeLn("Wheres the ground? Too foggy!");
                heightCheck();
            }
            break;
            case ("SNOW"): {
                this.coordinates.setHeight(this.coordinates.getHeight() - 15);
                Write.writeLn("Hope the snow cushions our landing!");
                heightCheck();
            }
            break;
        }

    }

    public void registerTower(WeatherTower weatherTower) {
        this.weatherTower = weatherTower;
        weatherTower.register(this);
        Write.writeLn("Balloon#" + this.name + " successfully registered!");
    }
}

class JetPlane extends Aircraft implements Flyable {
    private WeatherTower weatherTower;

    JetPlane(String name, Coordinates coordinates) {
        super(name, coordinates);
    }

    private boolean heightCheck() {
        if (this.coordinates.getHeight() <= 0) {
            Write.writeLn("JetPlane#" + name + "(" + id + "): Landing.");
            weatherTower.unregister(this);
            Write.writeLn("JetPlane#" + name + "(" + id + "): Unregistered from weather tower.");
            return (true);
        }
        return (false);
    }

    public void updateConditions() {
        Write.write("JetPlane#" + name + "(" + id + "): ");
        switch (weatherTower.getWeather(this.coordinates)) {
            case ("SUN"): {
                this.coordinates.setLatitude(this.coordinates.getLatitude() + 10);
                this.coordinates.setHeight(this.coordinates.getHeight() + 2);
                Write.writeLn("Suns up, so is my JetPlane... In my pants.");
                heightCheck();
            }
            break;
            case ("RAIN"): {
                this.coordinates.setLatitude(this.coordinates.getLatitude() + 5);
                Write.writeLn("It's raining. Better watch out for lightings.");
                heightCheck();
            }
            break;
            case ("FOG"): {
                this.coordinates.setLatitude(this.coordinates.getLatitude() + 1);
                Write.writeLn("Cant see! Eject! Eject! \"Omae wa mou SHINDEIRU!\"");
                heightCheck();
            }
            break;
            case ("SNOW"): {
                this.coordinates.setHeight(this.coordinates.getHeight() - 7);
                Write.writeLn("OMG! Winter is coming! Just like me...");
                heightCheck();
            }
            break;
        }
    }

    public void registerTower(WeatherTower weatherTower) {
        this.weatherTower = weatherTower;
        weatherTower.register(this);
        Write.writeLn("JetPlane#" + this.name + " successfully registered!");
    }
}

class Helicopter extends Aircraft implements Flyable {
    private WeatherTower weatherTower;

    Helicopter(String name, Coordinates coordinates) {
        super(name, coordinates);
    }

    private boolean heightCheck() {
        if (this.coordinates.getHeight() <= 0) {
            Write.writeLn("Helicopter#" + name + "(" + id + "): Landing.");
            weatherTower.unregister(this);
            Write.writeLn("Helicopter#" + name + "(" + id + "): unregistered from weather tower.");
            return (true);
        }
        if (this.coordinates.getHeight() > 100) {
            this.coordinates.setHeight(100);
        }
        return (false);
    }

    public void updateConditions() {
        Write.write("Helicopter#" + name + "(" + id + "): ");
        switch (weatherTower.getWeather(this.coordinates)) {
            case ("SUN"): {
                this.coordinates.setLongitude(this.coordinates.getLongitude() + 10);
                this.coordinates.setHeight(this.coordinates.getHeight() + 2);
                Write.writeLn("Goodbye darkness my old friend... Hello sun baby.");
                heightCheck();
            }
            break;
            case ("RAIN"): {
                this.coordinates.setLongitude(this.coordinates.getLongitude() + 5);
                Write.writeLn("Too much rain, such a shame...");
                heightCheck();
            }
            break;
            case ("FOG"): {
                this.coordinates.setLongitude(this.coordinates.getLongitude() + 1);
                Write.writeLn("\"Fog hides what the heart wont show\", what? There's fog");
                heightCheck();
            }
            break;
            case ("SNOW"): {
                this.coordinates.setHeight(this.coordinates.getHeight() - 12);
                Write.writeLn("My rotor is going to freeze!");
                heightCheck();
            }
            break;
        }
    }

    public void registerTower(WeatherTower weatherTower) {
        this.weatherTower = weatherTower;
        weatherTower.register(this);
        Write.writeLn("Helicopter#" + this.name + " successfully registered!");
    }

}
