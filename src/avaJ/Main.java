package avaj;

import avaj.FileDoesntExist;

public class Main {
    public static int           Ticks;
    public static WeatherTower  weatherTower = new WeatherTower();

    public static void  main ( String argv[]) {
        String          str;

		if (argv.length != 1) {
			throw new NoSceneFileError("Usage: java avaj/Main <scene file>");
		}

        Write.openFile("simulation.txt");
        LoadScenario.proccessSenarioFile(argv[0]);
        startSimulation();
        Write.closeFile();

        Read.openFile("simulation.txt");
        while ((str = Read.readLn()) != null) {
            System.out.println(str);
        }
        Read.closeFile();


    }

    public static void startSimulation() {
        weatherTower.changeWeather();
    }
}
