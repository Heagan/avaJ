package avaj;

class FileDoesntExist extends RuntimeException {
    FileDoesntExist( String message ) { 
        super(message);
    };
}

class FileOpeningError extends RuntimeException {
    FileOpeningError( String message ) { 
        super(message);
    };
}


class FileWritingError extends RuntimeException {
    FileWritingError( String message ) { 
        super(message);
    };
}

class FileFormatIncorrect extends RuntimeException {
    FileFormatIncorrect( String message ) { 
        super(message);
    };
}

class LineFormatIncorrect extends RuntimeException {
    LineFormatIncorrect( String message ) { 
        super(message);
    };
}

class UnknownError extends RuntimeException {
    UnknownError( String message ) { 
        super(message);
    };
}

class NoSceneFileError extends RuntimeException {
    NoSceneFileError( String message ) { 
        super(message);
    };
}
