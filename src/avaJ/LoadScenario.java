package avaj;

public class LoadScenario {

    public static void  proccessSenarioFile(String file) {
        String  line;
        String  slt[];

        Read.openFile(file);
        Main.Ticks = getTicks();
        while ((line = Read.readLn()) != null) {
            if ((slt = line.split(" ")).length != 5) {
                throw new FileFormatIncorrect("Senario File Error! Invalid Line: " + line);
            }
            if ((!Check.issDigit(slt[2])) || (!Check.issDigit(slt[3])) || (!Check.issDigit(slt[4]))) {
                throw new FileFormatIncorrect("Senario File Error! Invalid Coordinates");
            }
            AircraftFactory.newAircraft(slt[0], slt[1], Integer.parseInt(slt[2]), Integer.parseInt(slt[3]), Integer.parseInt(slt[4]));
        }
        Read.closeFile();
    }

    private static int getTicks() {
        String  line;

        line = Read.readLn();
        if (line == null)
            throw new FileOpeningError("Invalid Senario File! Was Unable to read from it....");
        if (!Check.issDigit(line))
            throw new FileFormatIncorrect("Senario doesn't specify tick count!");
        if (Integer.parseInt(line) <= 0)
            throw new FileFormatIncorrect("Senario Tick count must be positive!");
        return (Integer.parseInt(line));
    }

}
