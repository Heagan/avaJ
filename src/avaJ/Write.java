package avaj;
import java.io.*;

public class Write {
    private static FileWriter fileWriter;
    private static BufferedWriter bufferedWriter;

    public static void openFile( String file ) {
        try {
            fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
        }
        catch(IOException ex) {
            throw new FileOpeningError("Unable to open file '");
        }
    }

    public static int writeLn(String line) {
        try {
            bufferedWriter.write(line);
            bufferedWriter.newLine();
        } catch (IOException ex) {
            throw new FileWritingError("Error writing to file!");
        }
        return (1);
    }

    public static int write(String line) {
        try {
            bufferedWriter.write(line);
        } catch (IOException ex) {
            throw new FileWritingError("Error writing to file!");
        }
        return (1);
    }

    public static void closeFile() {
        try {
            bufferedWriter.close();
        } catch (IOException ex) {
            throw new FileOpeningError("Error closing file!");
        }
    }
}
