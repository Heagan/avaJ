package avaj;

public abstract class AircraftFactory {

    public static Flyable newAircraft(String type, String name, int longitude, int latitude, int height) {
        if (height <= 0 || height > 100) {
            throw new LineFormatIncorrect("Height of a craft must be between 0-100!\nExiting!");
        }
        switch (type) {
            case ("Balloon"): {
                Balloon balloon = new Balloon(name, new Coordinates(longitude, latitude, height));
                balloon.registerTower(Main.weatherTower);
                return (balloon);
            }
            case ("JetPlane"): {
                JetPlane jetPlane = new JetPlane(name, new Coordinates(longitude, latitude, height));
                jetPlane.registerTower(Main.weatherTower);
                return (jetPlane);
            }
            case ("Helicopter"): {
                Helicopter helicopter = new Helicopter(name, new Coordinates(longitude, latitude, height));
                helicopter.registerTower(Main.weatherTower);
                return (helicopter);
            }
            default: {
                throw new LineFormatIncorrect("Invalid Aircraft Specified! " + type + "?");
            }
        }
    }

}
