package avaj;

import java.util.ArrayList;
import java.util.List;

public abstract class Tower {
    private List<Flyable> observers = new ArrayList<>();

    public void register(Flyable flyable) {
        observers.add(flyable);
    }

    public  void unregister(Flyable flyable) {
        observers.remove(flyable);
    }

    protected void conditionsChanged() {
        for (int i = 0; i < observers.size(); i++) {
            observers.get(i).updateConditions();
        }
    }
}

class WeatherTower extends Tower {

    public String getWeather(Coordinates coordinates) {
        WeatherProvider weatherProvider;

        weatherProvider = WeatherProvider.getProvider();
        return (weatherProvider.getCurrentWeather(coordinates));
    }

    @Override
    public void register(Flyable flyable) {
        super.register(flyable);
    }

    @Override
    public void unregister(Flyable flyable) {
        super.unregister(flyable);
    }

    void changeWeather() {
        for (int i = 0; i < Main.Ticks; i++) {
            conditionsChanged();
        }
    }
}
